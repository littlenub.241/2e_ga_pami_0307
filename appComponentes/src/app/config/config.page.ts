import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-config',
  templateUrl: './config.page.html',
  styleUrls: ['./config.page.scss'],
})
export class ConfigPage implements OnInit {
  titulo="Configurações";

  configs =
  [
    {
      titulo: "Tema",
      conf1: "Escuro",
      icon1: "moon",
      conf2: "Automático",
      icon2: "bulb",
      conf3: "Claro",
      icon3: "sunny"
    },
  ];

  config2 =
  {
    titulo: "Conta",
      conf1: "Notificações",
      icon1: "notifications",
      conf2: "Conta",
      icon2: "person",
      conf3: "Sair",
      icon3: "exit"
  }


  constructor() { }

  ngOnInit() {
  }

}
