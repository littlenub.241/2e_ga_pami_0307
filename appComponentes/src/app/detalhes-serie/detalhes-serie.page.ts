import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalhes-serie',
  templateUrl: './detalhes-serie.page.html',
  styleUrls: ['./detalhes-serie.page.scss'],
})
export class DetalhesSeriePage implements OnInit {
  titulo = "CSI: Miami";
  series =
  [
    {
      titulo: "Crime Scene Investigation",
      subtitulo: "Miami",
      capa: 'https://http2.mlstatic.com/D_NQ_NP_812527-MLB27272704523_042018-O.jpg',
      texto: "Um time de investigadores forenses da Flórida usa tecnologia de ponta e métodos policiais tradicionais para solucionar crimes."
    }
  ]

  information =
  [
    {
      type: "Gênero",
      desc: "Policial, Drama, Procedimentos Policiais, Suspense"
    },
    {
      type: "Criador(es)",
      desc: "Anthony E. Zuiker, Carol Mendelsohn, Ann Donahue"
    },
    {
      type: "Idioma original",
      desc: "Inglês"
    },
  ]

  information2 = 
  [
    {
      type2: "Duração",
      desc21: "Temporadas: 10",
      desc22: "Episódios: 232",
      desc23: "Episódio: 40 - 45 minutos"
    }
  ]

  information3 =
  [
    {
      type3: "Elenco",
      desc3: "David Caruso, Emily Procter, Adam Rodriguez, Khandi Alexander, Rory Cochrane, Kim Delaney, Sofia Milos, Jonathan Togo, Rex Linn, Eva LaRue, Megalyn Echikunwoke, Eddie Cibrian, Omar Benson Miller"
    }
  ]

  
  constructor() { }

  ngOnInit() {
    
  }

}
